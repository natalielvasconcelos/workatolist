# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework import mixins, generics
from applications.core.models import Channel, Category
from applications.core.serializers import ChannelSerializer, CategorySerializer


class AbstractList(mixins.ListModelMixin, generics.GenericAPIView):

    def get(self, request, *args, **kwargs):
        """
        Return a list of all objects
        """
        return self.list(request, *args, **kwargs)


class ChannelList(AbstractList):
    """
    View to list all channels in the system
    """
    queryset = Channel.objects.all()
    serializer_class = ChannelSerializer


class CategoryList(AbstractList):
    """
    View to list all categories from a channel in the system
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def get_queryset(self):
        channel = self.kwargs.get('channel_token')
        return self.queryset.filter(channel__token=channel)


class CategoryParentList(mixins.ListModelMixin, generics.GenericAPIView):
    """
    View to return a single category with their parent categories and subcategories
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def get_queryset(self):
        token = self.kwargs.get('category_token')
        return list(Category.objects.get(token=token).get_family())