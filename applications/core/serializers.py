from rest_framework import serializers
from applications.core.models import Channel, Category

__author__ = 'Nataliel Vasconcelos'


class ChannelSerializer(serializers.ModelSerializer):
    """
    Channel Serializer
    """
    class Meta:
        model = Channel
        fields = ('id', 'name', 'token')


class CategorySerializer(serializers.ModelSerializer):
    """
    Category Serializer
    """
    class Meta:
        model = Category
        fields = ('id', 'name', 'token')


class CategoryParentSerializer(serializers.ModelSerializer):
    """
    Category Parent Serializer
    """
    class Meta:
        model = Category
        fields = ('id', 'name')