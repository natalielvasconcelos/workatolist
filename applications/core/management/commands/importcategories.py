import csv
from django.core.management.base import BaseCommand, CommandError
from applications.core.models import Channel, Category

__author__ = 'Nataliel Vasconcelos'


class Command(BaseCommand):
    help = 'Update the marketplaces categories from csv file'

    def add_arguments(self, parser):
        parser.add_argument('channel', type=str)
        parser.add_argument('csv', type=open)

    @staticmethod
    def add_channel_categories(channel, categories):
        category = None
        count = 0
        for count, category_name in enumerate(categories):
            category, created = list(Category.objects.get_or_create(channel=channel, name=category_name,
                                                                    parent=category))
        return count

    def handle(self, *args, **options):
        channel, created = Channel.objects.get_or_create(name=options['channel'])
        channel_delete = Category.objects.filter(channel__id=channel.id).delete()
        categories_csv = csv.reader(options['csv'])

        for row in categories_csv:
            category_name = row[0].split(' / ')
            self.add_channel_categories(channel, category_name)

        self.stdout.write(self.style.SUCCESS('Success! Marketplace updated!'))