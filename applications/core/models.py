# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os
import uuid
import binascii

from django.db import models
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel


class BaseModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    altered_at = models.DateTimeField(auto_now_add=True)
    token = models.CharField(max_length=10, unique=True)

    @staticmethod
    def create_hash():
        # hash generation
        return binascii.hexlify(os.urandom(5)).decode()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        token = self.create_hash()
        self.token = token
        super(BaseModel, self).save()

    class Meta:
        abstract = True


class Channel(BaseModel):
    """
    Channel's categories of marketplaces
    """
    name = models.CharField(verbose_name='Name', max_length=255)

    class Meta:
        ordering = ['created_at']
        verbose_name = 'Channel'
        verbose_name_plural = 'Channels'

    def __str__(self):
        return self.name


class Category(MPTTModel, BaseModel):
    """
    Category of products
    """
    name = models.CharField(verbose_name='Name', max_length=255)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)
    channel = models.ForeignKey(Channel)

    class Meta:
        ordering = ['created_at']
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    class MPTTMeta:
        order_insertion_by = ['id']

    def __str__(self):
        return self.name