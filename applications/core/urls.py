from django.conf.urls import url
from applications.core import views as core_views

__author__ = 'Nataliel Vasconcelos'

urlpatterns = [
    url(r'^channels/$', core_views.ChannelList.as_view(), name='channel_list'),
    url(r'^channel/(?P<channel_token>[\w_-]+)/$', core_views.CategoryList.as_view(), name='category_list_by_channel'),
    url(r'^category/(?P<category_token>[\w_-]+)/$', core_views.CategoryParentList.as_view(), name='parent_category_list'),
]